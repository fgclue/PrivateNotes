# PrivateNotes

**This Software is Outdated as HECK**

PrivateNotes is a note taking app that takes simplicity and speed to a whole new level. 

It offers you a quick and easy way to stay organised, capture your thoughts, reminders or anything that's on your mind, any time, anywhere. No extra unnecessary features, just notes.

### Build

Steps on how to build PrivateNotes:
- Make sure you have the latest version of Android Studio with Gradle v2.3.1 and the required Android SDK Tools installed (25.0.3 Build tools)
- Clone this repository
- Open Android Studio -> File -> Import Project
- Select build.gradle in PrivateNotes
- Go to Project Structure and make sure Android SDK and JDK paths are set
- Build -> Rebuild Project and Sync Gradle
- Good to go!
